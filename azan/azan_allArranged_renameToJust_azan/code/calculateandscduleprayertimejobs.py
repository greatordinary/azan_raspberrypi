from praytimes import PrayTimes
from datetime import date, datetime
import calendar
import os as os
from crontab import CronTab

commentusedtoflagcronjobs = 'azanjob'
commentusedtoflagcronjobs_controller = 'azanjob_controller'

dayint = date.today().weekday()
todaysname = calendar.day_name[dayint].lower()

root_dir = '/home/pi/Desktop/azan'

path_to_coordinates_file = os.path.join(root_dir, 'coordinates')

root_dir_for_code = os.path.join(root_dir, 'code')
path_to_azan_player = os.path.join(root_dir_for_code, 'azanplayer.py')
path_to_azan_controller = os.path.join(root_dir_for_code, 'calculateandscduleprayertimejobs.py')


rootdir_daily = os.path.join(root_dir, "daily")
rootdir_weekly = os.path.join(root_dir, "weekly")
rootdir_weekly_today = os.path.join(rootdir_weekly, todaysname) 

def createafile(fullpath):
    if not os.path.exists(fullpath):
        try:
            print("creating file: " + fullpath)
            f = open(fullpath, 'wt')
        finally:
            f.close()
            
def check_forcoordinates(fullpath):
    if not os.path.exists(fullpath):
        createafile(fullpath)
        try:
            file = open(fullpath,"w")
            file.write("lattitude = 38.96\n")
            file.write("longitude = -97\n")
            file.write("timezone = -6\n")
        finally: 
            file.close() 
        
        raise ValueError('Update coordinates at: ' +fullpath + ' and rerun ' + path_to_azan_controller )
            
def createallfoldersiftheydonotexist(directory):
    if not os.path.exists(directory):
        print("creating dir: " + directory)
        os.makedirs(directory)
    
def createcronjobforthecontroller_ifitdoesnotexist(command,cmt):
    jobexists = False
    my_cron = CronTab(user='pi')
    for job in my_cron:
        if job.comment == cmt:
            jobexists = True
        
    if jobexists:
        print("job already exists: " +cmt)
    else:
        job = my_cron.new(command = command, comment = cmt)
        job.hour.on(3)  #run at 3 am every day
        my_cron.write()

def createcronjob(command,hour,minute,jobcomment):
    #print("creating job: " +command)
    my_cron = CronTab(user='pi')
    job = my_cron.new(command = command, comment = jobcomment)
    job.hour.on(hour)
    job.minute.on(minute)
    my_cron.write()
    #print("job created: ")
    #print(job)
    
def deleteoldjobs(commentflag):
    my_cron = CronTab(user='pi')
    my_cron.remove_all(comment=commentflag)
    my_cron.write()
            
def printalljobs():
    my_cron = CronTab(user='pi')
    for job in my_cron:
        print(job)
    
def readcoordinates(path):
    try:
        file = open(path,"r")
        lines = file.readlines()
        print(file.readlines())
        lattitude = float(lines[0].split('=')[1])
        longitude = float(lines[1].split('=')[1])
        #timezone = float(lines[2].split('=')[1])
        timezone = int(round(((datetime.utcnow() - datetime.today()).seconds / 3600.00) * -1))
    finally:
        file.close()
        
    print ('coordinates from file: ' +str(lattitude) +' ' +str(longitude) +' ' +str(timezone))
    return (lattitude,longitude,timezone)
    
#create new jobs
prayTimes = PrayTimes('Jafari')

check_forcoordinates(path_to_coordinates_file)
#getcoordinates from the file:
(lattitude,longitude,timezone) = readcoordinates(path_to_coordinates_file)

times = prayTimes.getTimes(date.today(),(lattitude,longitude),timezone)
azantimes = {'fajr':times['fajr'], 'zuhr':times['dhuhr'], 'asr':times['asr'], 'maghrib':times['maghrib'], 'isha':times['isha']}

'''********Initialization : BEGIN creating all directories and files if they dont exist'''

#creates directory for each weekday and within each weekday it creates directory for each azan times
for day in ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']:
    for azan in azantimes.keys():
        weekdlydir = os.path.join(rootdir_weekly, day)
        weeklyanddaily = os.path.join(weekdlydir,azan)
        createallfoldersiftheydonotexist(weeklyanddaily)  

#creates daily azan directory
for azan in azantimes.keys():
    dailyazanpath = os.path.join(rootdir_daily, azan)
    createallfoldersiftheydonotexist(dailyazanpath)
'''********Initialization : END creating all directories and files if they dont exist'''

createcronjobforthecontroller_ifitdoesnotexist('python ' +path_to_azan_controller, commentusedtoflagcronjobs_controller) #this will be sceduled to run at 3am
deleteoldjobs(commentusedtoflagcronjobs)

print(azantimes)

#create jobs to run for each azan times
for azan in azantimes.keys():    
    cmd = 'python ' +path_to_azan_player +' ' +os.path.join(rootdir_daily, azan) +' ' +os.path.join(rootdir_weekly_today, azan)
    hr =  int(azantimes[azan].split(':')[0])
    min = int(azantimes[azan].split(':')[1])
    print("creating job for: " +azan)
    createcronjob(cmd,hr,min,commentusedtoflagcronjobs)

print('Scheduled jobs:')
printalljobs()

