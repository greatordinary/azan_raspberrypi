#!/usr/bin/python

import sys
import os as os
import time
from datetime import date
import calendar
from subprocess import Popen
import string
import fcntl
import subprocess, signal

secondstowait = 5

def filesInDirectory(directory):
    files = {}
    punc = string.punctuation + ' ' + string.ascii_letters
    for file in os.listdir(directory):
        index = next((i for i, ch  in enumerate(file) if ch in punc),None) #get the index of the first character from the list of punc
        key = int(file[0:index])
        files[key] = os.path.join(directory, file)
    sortedkeys = sorted(files.keys())
    return files,sortedkeys

def stopcurrentcronjob():
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()

    for line in out.splitlines():
        if 'omxplayer' in line:
        #if 'vlc' in line:
            print(line)
            pid = int(line.split(None, 1)[0])
            os.kill(pid, signal.SIGKILL)

def playfiles(directory):
    dic,sortedkeys = filesInDirectory(directory)
    for key in sortedkeys:
        #print("sleeping..now")
        time.sleep(secondstowait)
        #print("Playing: " +dic[key])
        omxp = Popen(['omxplayer','--vol','-700','-o','local',dic[key]])
        omxp.wait()
        #omxp = Popen(['cvlc',dic[key]])
        #stopcurrentcronjob()
    
def main():
    # argv holds the program name at index 0. That's why we start at 1.
    # three arguments will be passed:
    # 1st arg =  a full path to a directory where daily items will be played from
    # 2nd arg =  a full path for day and prayer time specific files 
    directoryToplaydailyfilesFrom =  sys.argv[1] 
    directoryToplaydayandprayertimespecificfilesFrom =  sys.argv[2]
    
    #hack to stop any currently running jobs.  to do : find a way to get a handle on currently running cron job and then kill it
    for i in range(10):
        stopcurrentcronjob()
        time.sleep(secondstowait)
        
    playfiles(directoryToplaydailyfilesFrom)
    playfiles(directoryToplaydayandprayertimespecificfilesFrom)
    

if __name__ == "__main__":
    main()
